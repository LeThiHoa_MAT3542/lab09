<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kiểm tra</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
   
    </style>
</head>
<body>
    <div class="main">
        <?php
            class Question
            {
                public $content;
                public $answerA;
                public $answerB;
                public $answerC;
                public $answerD;
                public $correctAnswer;
            }

            $question1 = new Question();
            $question1->content = 'Cau hoi 1';
            $question1->answerA = 'A.';
            $question1->answerB = 'B.';
            $question1->answerC = 'C.';
            $question1->answerD = 'D.';
            $question1->correctAnswer = 'B';

            $question2 = new Question();
            $question2->content = 'Cau hoi 2';
            $question2->answerA = 'A';
            $question2->answerB = 'B';
            $question2->answerC = 'C';
            $question2->answerD = 'D';
            $question2->correctAnswer = 'C';

            $question3 = new Question();
            $question3->content = 'Cau hoi 3';
            $question3->answerA = 'A';
            $question3->answerB = 'B';
            $question3->answerC = 'C';
            $question3->answerD = 'D';
            $question3->correctAnswer = 'C';

            $question4 = new Question();
            $question4->content = 'Cau hoi 4';
            $question4->answerA = 'A';
            $question4->answerB = 'B';
            $question4->answerC = 'C';
            $question4->answerD = 'D';
            $question4->correctAnswer = 'A';

            $question5 = new Question();
            $question5->content = 'Cau hoi 5';
            $question5->answerA = 'A';
            $question5->answerB = 'B';
            $question5->answerC = 'C';
            $question5->answerD = 'D';
            $question5->correctAnswer = 'B';

            $question6 = new Question();
            $question6->content = 'Cau hoi 6';
            $question6->answerA = 'A';
            $question6->answerB = 'B';
            $question6->answerC = 'C';
            $question6->answerD = 'D';
            $question6->correctAnswer = 'B';

            $question7 = new Question();
            $question7->content = 'Cau hoi 7';
            $question7->answerA = 'A';
            $question7->answerB = 'B';
            $question7->answerC = 'C';
            $question7->answerD = 'D';
            $question7->correctAnswer = 'D';

            $question8 = new Question();
            $question8->content = 'Cau hoi 8';
            $question8->answerA = 'A.';
            $question8->answerB = 'B.';
            $question8->answerC = 'C.';
            $question8->answerD = 'D.';
            $question8->correctAnswer = 'C';

            $question9 = new Question();
            $question9->content = 'Cau hoi 9';
            $question9->answerA = 'A.';
            $question9->answerB = 'B.';
            $question9->answerC = 'C.';
            $question9->answerD = 'D';
            $question9->correctAnswer = 'C';

            $question10 = new Question();
            $question10->content = 'Cau hoi 10';
            $question10->answerA = 'A.';
            $question10->answerB = 'B.';
            $question10->answerC = 'C';
            $question10->answerD = 'D';
            $question10->correctAnswer = 'D';


            $questions = array($question1, $question2, $question3, $question4, $question5, $question6, $question7, $question8, $question9, $question10);

        ?>

        <?php
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $cookie_name = "choices";
                $cookie_value = $_POST;

                setcookie($cookie_name, json_encode($cookie_value), time()+3600);

                header("Location: SecondPage.php");
            }
        ?>
        <div class="wrapper">
            <h3 class="title">Bài Kiểm tra!</h3>
            <form method="post">
                <?php
                    for ($i = 0; $i < 5; $i++) {
                        echo '<div class="question">
                                <h4 class="question__content">' . ($i+1) . '. ' . $questions[$i]->content . '</h4>
                                <input required type="radio" id="answerA' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="A">
                                <label for="answerA' . ($i+1). '">A. ' . $questions[$i]->answerA . '</label><br>
                                <input type="radio" id="answerB' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="B">
                                <label for="answerB' . ($i+1). '">B. ' . $questions[$i]->answerB . '</label><br>
                                <input type="radio" id="answerC' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="C">
                                <label for="answerC' . ($i+1). '">C. ' . $questions[$i]->answerC . '</label><br>
                                <input type="radio" id="answerD' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="D">
                                <label for="answerD' . ($i+1). '">D. ' . $questions[$i]->answerD . '</label><br>
                            </div>';
                    }
                ?>

                <br>
                <input type="submit" class="btn-submit" value="Next">
            </form>

        </div>

    </div>

</body>
</html>
